Snippet
=======

Download the `getset.sublime-snippet` file from this repository, or copy the
code into a new snippet with `Tools` > `New Snippet`:

```
<snippet>
    <content><![CDATA[
/**
 * @return ${1:mixed}
 */
public function get${2:PropertyName}()
{
    return \$this->${2/(^.)/\l$1/};
}

/**
 * @param ${1}
 */
public function set${2}(\$${2/(^.)/\l$1/})
{
    \$this->${2/(^.)/\l$1/} = \$${2/(^.)/\l$1/};
}
]]></content>
    <tabTrigger>getset</tabTrigger>
    <scope>source.php</scope>
</snippet>
```

Save the file into your `Packages\User` folder.

Usage
=====

Type `getset` + `[tab]` to insert the snippet. Type the property type (`int`,
`bool`...), press `[tab]` again and type the property name beginning with a
capital letter.
